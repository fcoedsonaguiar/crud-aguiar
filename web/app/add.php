<?php

require('../config/connection.php');
require('funcionarios.php');

class add {

    private $funcionario;
    private $connection;
    private $firstName;
    private $lastName;
    private $email;
    private $address;
    private $active;

    public function __construct()
    {
        $this->connection = new connection();
        $this->connection->createTable();

        $this->firstName = $_POST['firstname'];
        $this->lastName = $_POST['lastname'];
        $this->email = $_POST['email'];
        $this->address = $_POST['address'];

        $functionario = new funcionarios(
            $this->firstName,
            $this->lastName,
            $this->email,
            $this->address,
            1
        );

        $this->funcionario = $functionario;
    }

    public function insertData() {

        $insert = "INSERT INTO funcionarios
              (
                first_name,
                last_name,
                email,
                address,
                create_date
              ) 
              VALUES
              (
                "
            . "'" . $this->funcionario->getFirstName() . "'" . ","
            . "'" . $this->funcionario->getLastName() . "'" . ","
            . "'" . $this->funcionario->getEmail() . "'" . ","
            . "'" . $this->funcionario->getAddress() . "'" . ","
            . "'" . date('Y-m-d H:i:s') . "'" .
            ")";

        if (mysqli_query($this->connection->getCon(), $insert) === true){
            header("Location:/index.php");
        } 
        else {
            echo $this->connection->getCon()->error;
        }

        mysqli_close($this->connection->getCon());
    }

}


$add = new add();
$add->insertData();