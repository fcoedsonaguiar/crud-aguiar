<?php

//define('app/connection', dirname(__FILE__));
require ('funcionarios.php');
require_once '../config/connection.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);


class deleteData
    {	

	    private $connection;
	    private $employees;

	    public function __construct() {	

	    	$this->employees = new funcionarios($_POST['firstname'],
				    							$_POST['lastname'],	
												$_POST['email'],
												$_POST['address'],
												$_POST['active']);		

	    	$this->employees->setId($_POST['id']);
	    	$this->connection = new connection();
		}

	    public function delete () {

			$id = $this->employees->getId();
        	$sql = "DELETE FROM ". $this->connection->getTableName() . " WHERE id = ? ";
			
			if($stmt = $this->connection->getCon()->prepare($sql)) {
				$stmt->bind_param('i', $id);
				$stmt->execute();
			} else {
			    $error = $this->connection->getCon()->errno . ' ' . $this->connection->getCon()->error;
			    echo $error;
			}
		
			header("Location:/index.php");
			
			$stmt->close();

   			return $stmt;
       	}
    }

$delete = new deleteData();
$delete->delete();