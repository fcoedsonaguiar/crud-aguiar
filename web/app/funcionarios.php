<?php

//namespace app\funcionarios;

class funcionarios
{
    protected $id;
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $address;
    protected $fatima;
    protected $maraponga;
    protected $active;

    public function __construct
    (
        $firstName,
        $lastName,
        $email,
        $address,
        $fatima,
        $maraponga,
        $active
    )
    {   
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->address = $address;
        $this->fatima = $fatima;
        $this->maraponga = $maraponga;
        $this->active = $active;
    }


    public function getId() {
        return $this->id;
    }

    public function getFirstName() {
        return $this->firstName;
    }

    public function getLastName() {
        return $this->lastName;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getAddress() {
        return $this->address;
    }

    public function getfatima() {
        return $this->fatima;
    }
    public function getMaraponga() {
        return $this->maraponga;
    } 
    public function getActive() {
        return $this->active;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    public function setLastName($lastName) {
        $this->lastName = $lastName;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function setAddress($address) {
        $this->address = $address;
    }
    public function setfatima($fatima) {
            $this->fatima = $fatima;
        }
    public function setMaraponga($maraponga) {
            $this->maraponga = $maraponga;
        }    
    public function setActive($active) {
        $this->active = $active;
    }
}