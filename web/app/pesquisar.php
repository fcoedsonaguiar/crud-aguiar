<?php
require('../config/connection.php');
require('funcionarios.php');

class searchData {

    private $connection;
    private $employees;

    private $parangaba;
    private $parquelandia;
    private $centro;
    private $joao;
    private $fatima;
    private $maraponga;

    public function __construct()
    {
            $this->employees = new funcionarios($_POST['parangaba'],
                                                $_POST['parquelandia'], 
                                                $_POST['centro'],
                                                $_POST['joao'],
                                                $_POST['fatima'],
                                                $_POST['maraponga']);  
        
            $this->connection = new connection();
    }

    public function search () {

            $this->parangaba->getFirstName();
            $this->parquelandia->getLastName();
            $this->centro->getEmail();
            $this->joao->getAddress();
            $this->fatima->getFatima();
            $this->maraponga = 'maraponga';

            $sql = "SELECT * FROM " . $this->connection->getTableName() . " WHERE address = '?' OR address = '?' OR address = '?' OR address = '?' OR address = '?' OR address = '?' ";
            
            if($stmt = $this->connection->getCon()->prepare($sql)) {
                $stmt->bind_param('ssssss', $parangaba, $parquelandia, $centro, $joao, $fatima, $maraponga);
                $stmt->execute();
            } else {
                $error = $this->connection->getCon()->errno . ' ' . $this->connection->getCon()->error;
                echo $error;
            }
        
            //header("Location:/pesquisar.html");
            
            $stmt->close();
            return $stmt;
        }
    
}
$search = new searchData();
$search->search();