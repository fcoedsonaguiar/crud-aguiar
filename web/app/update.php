<?php

//define('app/connection', dirname(__FILE__));
require ('funcionarios.php');
require_once '../config/connection.php';

class updateData
    {	

	    private $connection;
	    private $employees;

	    public function __construct() {	

	    	$this->employees = new funcionarios($_POST['firstname'],
				    							$_POST['lastname'],	
												$_POST['email'],
												$_POST['address'],
												$_POST['active']);		

			
	    	$this->employees->setId($_POST['id']);

	    	$this->connection = new connection();
		}

	    public function update () {

	    	$sqlUpdate = "UPDATE " . $this->connection->getTableName() . " SET " 
	        . 	"first_name = "	. '"' . $this->employees->getFirstName() . '"'
	        . ", " . "last_name = " . '"' . $this->employees->getLastName() . '"'
	        . ", " . "email = " . '"' . $this->employees->getEmail() . '"' 
	        . ", " . "active = " . '"' . $this->employees->getActive() . '"'
	        . ", " . "address = " . '"' . $this->employees->getAddress() . '"'
	        . "where id = " . '"' . $this->employees->getId() . '"' . ";";

	        
			if (mysqli_query($this->connection->getCon(), $sqlUpdate) === true) {
				$sqlResult = mysqli_query($this->connection->getCon(), $sqlUpdate);
		       	header ('Location: /index.php');		
			} else {
            	echo $this->connection->getCon()->error;
        	}

    			mysqli_close($this->connection->getCon());
    			
       			return $sqlResult;
       	}
    }

$updateData = new updateData();
$updateData->update();