<?php
/*** nullify any existing autoloads ***/
spl_autoload_register(null, false);

/*** specify extensions that may be loaded ***/
spl_autoload_extensions('.php');

/*** class Loader ***/
function classLoader($class)
{
	//$filename = ucfirst(strtolower($class) . '.php') ; 
	$filename = $class.'.php' ;
	$dirs = array(__DIR__ .'/' , __DIR__.'/index/') ;
	foreach ($dirs as $dir) 
	{
		if (file_exists($dir.$filename)) 
		{
			include $dir.$filename ;
			break ; 
		}
	} 
}

/*** register the loader functions ***/
spl_autoload_register('classLoader');
?>