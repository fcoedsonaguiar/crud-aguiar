<?php

class connection {

    protected $hostname;
    protected $username;
    protected $password;
    protected $database;
    protected $conn;
    protected $tableName;

    public function __construct()
    {
        $this->hostname = "db:3306";
        $this->username = "cruduser";
        $this->password = "crudpass";
        $this->database = "cruddb";
        $this->tableName = "funcionarios";
        return $this->conn = mysqli_connect($this->hostname, $this->username, $this->password, $this->database);
    }

    public function getHostName()
    {
        return $this->hostname;
    }

    public function getUserName()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function getCon() {
        return $this->conn;
    }

    public function getTableName() {
        return $this->tableName;
    }


    public function createTable() {
        if ($this->getCon()) {
            try {
                $createTable = "CREATE TABLE IF NOT EXISTS funcionarios (
                                id INT(7) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                                first_name VARCHAR(30) NOT NULL,
                                last_name VARCHAR(30) NOT NULL,
                                email VARCHAR(30) NOT NULL,
                                address VARCHAR(30) NOT NULL,
                                active BOOLEAN NOT NULL DEFAULT 1,
                                create_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
                                )";

                mysqli_query($this->getCon(), $createTable);

            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        } else {
            die('Could not connect: '.mysqli_connect_error());
        }
    }
}